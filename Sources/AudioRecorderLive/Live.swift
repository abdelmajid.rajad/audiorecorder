import AudioRecorder
import Combine
import AVFoundation

var audioRecorder: AVAudioRecorder?
let audioSession = AVAudioSession.sharedInstance()
var currentFileURL: URL?

let settings = [
    AVFormatIDKey: Int(kAudioFileMP3Type),
    AVSampleRateKey: 12000,
    AVNumberOfChannelsKey: 1,
    AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
]


extension AudioClient {
    
    class Delegate: NSObject, AVAudioRecorderDelegate {
        
        let subscriber: Publishers.Create<AudioClient.Action, AudioClient.Error>.Subscriber
        
        public init(subscriber: Publishers.Create<AudioClient.Action, AudioClient.Error>.Subscriber) {
            self.subscriber = subscriber
        }
        
        func audioRecorderDidFinishRecording(
            _ recorder: AVAudioRecorder,
            successfully flag: Bool) {
            subscriber.send(.didFinishRecording(
                                fileURL: flag ? currentFileURL: nil))
        }
        
        private func audioRecorderEncodeErrorDidOccur(
            _ recorder: AVAudioRecorder,
            error: Error?) {
            subscriber.send(.encodeErrorDidOccur(error: error))
        }
    }
        
    public static var live: Self {
        Self(
            recordPermission: audioSession.recordPermission,
            requestRecordPermission: .future { promise in
                do {
                    try audioSession.setCategory(.playAndRecord, mode: .default)
                    try audioSession.setActive(true)
                    audioSession.requestRecordPermission() {
                        promise(.success($0))
                    }
                } catch {
                    promise(.failure(.requestingPermission))
                }
            },
            record: { fileURL in
                .create { subscriber in
                    do {
                        currentFileURL = fileURL
                        audioRecorder = try AVAudioRecorder(
                            url: fileURL,
                            settings: settings
                        )                        
                        audioRecorder?.delegate = Delegate(subscriber: subscriber)
                        audioRecorder?.record()
                        subscriber.send(.didStartRecording)
                    } catch {
                        subscriber.send(
                            .didStopRecording(error:
                            .didStopRecording(description: error.localizedDescription)))
                    }
                    return AnyCancellable {
                        audioRecorder?.stop()
                        audioRecorder = nil
                    }
                }
            },
            end: {
                audioRecorder?.stop()
                audioRecorder = nil
            }
        )
    }
}

extension AnyPublisher {
    static func future(
        _ attemptToFulfill: @escaping (@escaping (Result<Output, Failure>) -> Void) -> Void
    ) -> AnyPublisher {
        Deferred {
            Future { callback in
                attemptToFulfill { result in callback(result) }
            }
        }.eraseToAnyPublisher()
    }
}


func getDocumentsDirectory() -> URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
}
