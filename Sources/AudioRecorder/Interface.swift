import Combine
import AVFoundation


public struct AudioClient {
    
    public let recordPermission: AVAudioSession.RecordPermission
    public let requestRecordPermission: AnyPublisher<Bool, Error>
    public let record: (_ to: URL) -> AnyPublisher<Action, Error>
    public let end: () -> Void
    
    public init(
        recordPermission: AVAudioSession.RecordPermission,
        requestRecordPermission: AnyPublisher<Bool, Error>,
        record: @escaping (URL) -> AnyPublisher<Action, Error>,
        end: @escaping () -> Void
    ) {
        self.recordPermission = recordPermission
        self.requestRecordPermission = requestRecordPermission
        self.record = record
        self.end = end
    }
    
    public enum Error: Swift.Error, Equatable {
        case requestingPermission
        case didStopRecording(description: String)
    }
    
    public enum Action {
        case didStartRecording
        case didEndRecoding(file: URL)
        case didStopRecording(error: Error?)
        case didFinishRecording(fileURL: URL?)
        case encodeErrorDidOccur(error: Error?)
    }
    
}
