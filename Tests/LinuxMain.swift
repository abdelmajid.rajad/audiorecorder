import XCTest

import AudioRecorderTests

var tests = [XCTestCaseEntry]()
tests += AudioRecorderTests.allTests()
XCTMain(tests)
