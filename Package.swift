// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AudioRecorderClient",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "AudioRecorder",
            type: .dynamic,
            targets: ["AudioRecorder"]),
        .library(
            name: "AudioRecorderLive",
            type: .dynamic,
            targets: ["AudioRecorderLive"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "AudioRecorder",
            dependencies: []),
        .target(
            name: "AudioRecorderLive",
            dependencies: ["AudioRecorder"]),
        .testTarget(
            name: "AudioRecorderTests",
            dependencies: ["AudioRecorder"]),
    ]
)
